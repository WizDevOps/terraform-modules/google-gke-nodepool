variable "project_id" {}
variable "region" {}
variable "nodes_per_zone" {}
variable "autoscaling_nodes_count" {
  default = {
    min = 5
    max = 20
  }
}
variable "oauth_scopes" {}
variable "machine_type" {}
variable "gke_cluster" {}
variable "gke_nodepool_service_account" { type = "string" }
variable "nodepool_labels" { type = "map" }
variable "nodepool_tags" {
  description = "Match them with the labels from Firewall in order to separate between private/public network"
  type        = "list"
}