terraform {
  backend "gcs" {}
}
provider "google-beta" {}
terraform {
  required_version = ">= 0.12"
}

data "google_container_cluster" "default" {
  name = var.gke_cluster
}

resource "google_container_node_pool" "alpha" {
  provider           = "google-beta"
  name               = "${data.google_container_cluster.default.name}-alpha"
  location           = var.region     // TODO: move this to provider
  project            = var.project_id // TODO: move this to provider
  cluster            = data.google_container_cluster.default.name
  initial_node_count = var.nodes_per_zone

  autoscaling {
    min_node_count = var.autoscaling_nodes_count.min
    max_node_count = var.autoscaling_nodes_count.max
  }

  management {
    auto_repair  = true
    auto_upgrade = true
  }

  node_config {
    preemptible     = true
    machine_type    = var.machine_type
    image_type      = "COS"
    service_account = var.gke_nodepool_service_account

    metadata = {
      disable-legacy-endpoints = "true"
    }

    labels = var.nodepool_labels

    tags = var.nodepool_tags

    oauth_scopes = var.oauth_scopes
  }

  lifecycle {
    ignore_changes = [initial_node_count]
  }

  timeouts {
    create = "30m"
    update = "30m"
    delete = "30m"
  }
}
