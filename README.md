# GCP GKE Nodepool
Spins up Google Kubernetes Engines (GKE) Nodepools for the GKE Cluster

## Variables

| Variable | Type | Description | Example |
| -------- | ---- | ----------- | ------- |
| `project_id` | `string` | GCP Project ID | `sample-project123` |
| `region` | `string` | GCP Region of the project | `europe-west4` |
| `nodes_per_zone` | `number` | How many nodes you want per AZ | `3` |
| `autoscaling_nodes_count` | `map` | Limit no. of nodes for autoscaling | `{min=5, max=20}` |
| `oauth_scopes` | `list` | Grant some permissions to the Nodepool | `["https://www.googleapis.com/auth/cloud-platform",]`
| `machine_type` | `string` | Size of VM | `f1-micro` |
| `gke_cluster` | `string` | Reference to the GKE Cluster created in advance | `module.gke_cluster.gke_cluster_name` |
| `gke_nodepool_service_account` | `string` | Service Account for the Nodepool | `module.gke_service_accounts.email` |
| `nodepool_labels` | `map` | Label the Nodepool accordingly | `{env = "dev"}` |
| `nodepool_tags` | `list` | Tag the nodepool for controlling Network access in the VPC | `["private-pool-sample"]` |

## Outputs
_None_

## Usage

```hcl
module "gke_nodepool" {
  source = "git::https://gitlab.com/WizDevOps/terraform-modules/google-gke-nodepool.git?ref=v0.0.1"

  project_id                   = local.project_id
  region                       = local.region
  nodes_per_zone               = 3
  autoscaling_nodes_count = {
    min = 5
    max = 20
  }
  gke_cluster                  = module.gke_cluster.gke_cluster_name
  gke_nodepool_service_account = module.gke_service_accounts.email
  machine_type                 = "f1-micro"
  oauth_scopes = [
    "https://www.googleapis.com/auth/cloud-platform",
  ]
  nodepool_labels = {
    private-pools-sample = true
  }
  nodepool_tags = [
    module.vpc_network.private,
    "private-pool-sample"
  ]
}
```
