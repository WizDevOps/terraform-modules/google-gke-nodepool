# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.1.0-rc] - 2019-09-09
### Fixed
- e86de417 logical dependency between Nodepool -> Cluster; the Nodepool wasn't waiting for the
  cluster to be created before, add a logical dependency between them

## [0.0.2] - 2019-09-08
### Added
- 8f11627b option to change no. of nodes for autoscaling (min/max nodes)

### Fixed
- broken links in README

## [0.0.1] - 2019-09-07
### Added
- CHANGELOG + LICENCE + README

[Unreleased]: https://gitlab.com/WizDevOps/terraform-modules/google-gke-nodepool/compare/v0.1.0-rc...master
[0.1.0-rc]: https://gitlab.com/WizDevOps/terraform-modules/google-gke-nodepool/compare/v0.0.2...v0.1.0-rc
[0.0.2]: https://gitlab.com/WizDevOps/terraform-modules/google-gke-nodepool/compare/v0.0.1...v0.0.2
[0.0.1]: https://gitlab.com/WizDevOps/terraform-modules/google-gke-nodepool/tree/v0.0.1